\select@language {german}
\contentsline {section}{I Inhaltsverzeichnis}{I}{section*.1}
\contentsline {section}{II Abk\"urzungsverzeichnis}{II}{section*.2}
\contentsline {section}{III Abbildungsverzeichnis}{III}{section*.3}
\contentsline {section}{IV Tabellenverzeichnis}{IV}{section*.4}
\contentsline {section}{V Symbolverzeichnis}{V}{section*.5}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Grenzen der Kleinst-Quadrate-Sch\"atzung und Subset Selection}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Multiple lineare Regressionsmodelle und die KQ-Methode}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Bewertung der Qualit\"at einer Sch\"atzung}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Verfahren zur Selektion von Subsets}{9}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Best Subset Selection}{9}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Stepwise Selection und der Forward Stagewise Ansatz}{11}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}LASSO - Least Absolut Shrinkage and Selection Operator}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Formale Gestalt und L\"osung des Verfahrens}{12}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Die $L_{1}$-Norm und die Geometrie des LASSO-Ansatzes}{14}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Spezialfall einer orthonormalen Designmatrix}{16}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}L\"osung bei beliebiger Designmatrix}{18}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Verfahren zur Bestimmung des Shrinkage-Parameters}{21}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}K-fold Cross-validation}{21}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Generalized Cross-validation}{21}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Sch\"atzung der Standardfehler}{21}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Vergleich der Qualit\"at der Sch\"atzresultate}{21}{subsection.3.4}
\contentsline {section}{\numberline {4}Title of section 2}{23}{section.4}
\contentsline {subsection}{\numberline {4.1}\texttt {Subsection 1}}{23}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}\texttt {Subsection 2}}{23}{subsection.4.2}
\contentsline {section}{\numberline {5}Title of section 3}{24}{section.5}
\contentsline {subsection}{\numberline {5.1}\texttt {Subsection 1}}{24}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}\texttt {Subsection 2}}{24}{subsection.5.2}
\contentsline {section}{VI Literaturverzeichnis}{VI}{section*.8}
\contentsline {section}{VII Appendix}{VIII}{section*.9}
