# Installation zoo
install.packages("zoo")

setwd("Z:/Projekte Analytics/Calvendo/Rohdaten/CSV-Files Output")

library(zoo)
# f?r Freq over 10 (4283 Keywords) dann folgende Datei
# "Kalender_Auswertung_Version_2015-08-18_10-49 (Freq over 10).csv" 
data_original  <- read.csv(file="Kalender_Auswertung_Version_mit_UserQuote_2015-08-19_18-03 (Freq over 100).csv",
                  header=TRUE, 
                  sep=",",
                  dec=".", 
                  encoding="utf-8",
                  stringsAsFactors = FALSE)




dim(data)
data_original <- data_original[,2:length(data_original)]
data <- data_original

data$Veroeffentlichungsdatum <- as.Date(data$Veroeffentlichungsdatum)
Veroeffentlichungsquartal <- format(as.yearqtr(data$Veroeffentlichungsdatum), format="%q")
Veroeffentlichungsmonat <- substr(data$Veroeffentlichungsdatum,6,7)
data <- data.frame(data[,1:7],Veroeffentlichungsquartal,Veroeffentlichungsmonat, data[,8:length(data)])
rm(Veroeffentlichungsmonat,Veroeffentlichungsquartal)


# Ausrei?er, Nullverk?ufe und NA entfernen von TotalSales
(subset(data[,c(2:4,7,10:13)],data$TotalSales == max(data$TotalSales)))
arrange(subset(data[,c(2:4,7,10:13)],data$TotalSales %in% tail(sort(data$TotalSales),7)),desc(data$TotalSales))
arrange(subset(data_original[,c(3:4,6:13)],data_original$TotalSales %in% tail(sort(data_original$TotalSales),10)),desc(TotalSales))

data <- subset(data, !is.na(TotalSales))
data <- subset(data, data$TotalSales != max(data$TotalSales))

nrow(subset(data, data$TotalSales == 0))
nrow(subset(data_original, data_original$TotalSales == 0))
data <- subset(data, data$TotalSales != 0)

nrow(subset(data, data$TotalSales <= 100))
nrow(subset(data, data$TotalSales > 100))
dataUpTo100 <- subset(data, data$TotalSales <= 100)

nrow(subset(data, data$TotalSales <= 40))
nrow(subset(data, data$TotalSales > 40))
dataUpTo40 <- subset(data, data$TotalSales <= 40)

# Verlagsnahe User entfernen und deren Titel

#User               ID
#Raphael Maurer   19643
#Lit0	              181
#Lit1	              218
#Lit2	              219
#Lit3	              798
#Lit4	            14223
#Lit5	            20728
#Lit6	            21762
#Lit7	            23392
#Lit8_Frankreich  39902
#Lit9_England	    39903
#Lit10_geotop	    40124
#Lit11_imago	    40128
#Lit12_Planer	    41902
#Lit13_Geburtstag 41903
#Lit14_Okapia	    41934
#Lit15_Arkivi	    43211
#Lit16_bpk	      43212
#Lit17_interfoto  46116

nrow(subset(data[,c(1,2,4,15,16)],data$PublisherID == 19643))
nrow(subset(data[,4:6],data_original$PublisherID %in% c(19643,181,218,219,798,14223,20728,21762,
                                           23392,39902,39903,40124,40128,
                                           41902,41903,41934,43211,43212,46116)))

# Generiere Data Verlagsfrei ########################################
dataVerlagsfrei1 <- subset(data_original, !is.na(PublisherID))
dataVerlagsfrei1 <- subset(data_original,!(data_original$PublisherID %in% c(19643,181,218,219,798,14223,20728,21762,
                                                  23392,39902,39903,40124,40128,
                                                  41902,41903,41934,43211,43212,46116)))
dataVerlagsfrei1$Veroeffentlichungsdatum <- as.Date(dataVerlagsfrei1$Veroeffentlichungsdatum)
Veroeffentlichungsquartal <- format(as.yearqtr(dataVerlagsfrei1$Veroeffentlichungsdatum), format="%q")
Veroeffentlichungsmonat <- substr(dataVerlagsfrei1$Veroeffentlichungsdatum,6,7)
dataVerlagsfrei1 <- data.frame(dataVerlagsfrei1[,1:7],Veroeffentlichungsquartal,Veroeffentlichungsmonat, dataVerlagsfrei1[,8:length(dataVerlagsfrei1)])
rm(Veroeffentlichungsmonat,Veroeffentlichungsquartal)

arrange(subset(dataVerlagsfrei1[,c(3:4,6:13)],dataVerlagsfrei1$TotalSales %in% tail(sort(dataVerlagsfrei1$TotalSales),10)),desc(TotalSales))
nrow(subset(dataVerlagsfrei1, dataVerlagsfrei1$TotalSales == 0))
nrow(subset(dataVerlagsfrei1, dataVerlagsfrei1$TotalSales <= 100 & dataVerlagsfrei1$TotalSales != 0))
#######################################################################

# durchschnittliche Sales pro Jahr
#i=1
#head(substr(dataVerlagsfrei1[i,7],1,4))
rm(df_test, av.Sales)
av.Sales <- c()
x0 <- c()
x1 <- c()
x2 <- c()
x3 <- c()
x4 <- c()
x5 <- c()
x6 <- c()
x7 <- c()
x8 <- c()
x9 <- c()
x10 <- c()
df_test <- 0
Nenner <- 0
sum.All <- 0
kor <- 0
for(i in 1:nrow(dataVerlagsfrei1)){
  sum.All <- sum(dataVerlagsfrei1[i,10],dataVerlagsfrei1[i,11],dataVerlagsfrei1[i,12],dataVerlagsfrei1[i,13])
  V.year <- substr(dataVerlagsfrei1[i,7],1,4)
  DaysLeft <- 0
  
  if(as.Date(paste(V.year,"-06-30", sep="")) < dataVerlagsfrei1[i,7])
      {
    DaysLeft <- as.numeric(as.Date(paste(V.year,"-12-31", sep="")) - dataVerlagsfrei1[i,7])
      }
  else (DaysLeft <- 365.25)
  
  
  if(sum.All == 0)
  {
    av.Sales[i] <- 0
  }
  
  else(
    for(j in 13:10){
      if(dataVerlagsfrei1[i,j]!=0){
        Y.last.sold <- substr(colnames(dataVerlagsfrei1)[j],6,9) 
          if(Y.last.sold == 2015){kor <- ((31+28)/365.25)}
          if(Y.last.sold != 2015){kor <- 0}
          Nenner <- as.numeric(Y.last.sold) - as.numeric(V.year) + (DaysLeft/365.25) + kor 
          if(Nenner < 1){Nenner <- 1}
          av.Sales[i] <- sum.All / Nenner
          x1[i] <- dataVerlagsfrei1[i,10] 
          x2[i] <- dataVerlagsfrei1[i,11] 
          x3[i] <- dataVerlagsfrei1[i,12]
          x4[i] <- dataVerlagsfrei1[i,13]
          x5[i] <- sum.All
          x6[i] <- Nenner
          x7[i] <- av.Sales[i]
          x8[i] <- DaysLeft
          x9[i] <- (DaysLeft/365.25)
          x10[i] <- kor
          break
        }
    }
  )
}
df_test <- data.frame(Sales2012 = x1,Sales2013 = x2,Sales2014 = x3,Sales2015 = x4,SumAll = x5,Nenner = x6,av.Sales = x7, Daysleft = x8, AnteilDays = x9,kor = x10)

dataVerlagsfrei2 <- data.frame(dataVerlagsfrei1[1:4],av.Sales,dataVerlagsfrei1[5:length(dataVerlagsfrei1)])

#  for(j in 13:10){
#    year <- substr(dataVerlagsfrei1[i,7],1,4)
#    if(dataVerlagsfrei1[i,j]==0){
#      
#      }
#    (substr(colnames(dataVerlagsfrei1)[j],6,9))
 






#######################################################################

par(mfrow=c(1,4))
boxplot(data_original$TotalSales,data=data_original, main="Originaldatenmenge", 
        xlab=paste(nrow(data_original),"Datens?tze"), ylab="TotalSales pro Urprojekt",
        cex.lab=1.5, cex.axis=1.5, cex.main=1.5, cex.sub=1.5)
boxplot(data$TotalSales,data=data, main="Ohne Max Wert und Ohne Null", 
        xlab=paste(nrow(data),"Datens?tze"), ylab="TotalSales pro Urprojekt",
        cex.lab=1.5, cex.axis=1.5, cex.main=1.5, cex.sub=1.5)
boxplot(dataUpTo100$TotalSales,data=dataUpTo100, main="Sales bis zu 100 (ohne 0)", 
        xlab=paste(nrow(dataUpTo100),"Datens?tze"), ylab="TotalSales pro Urprojekt",
        cex.lab=1.5, cex.axis=1.5, cex.main=1.5, cex.sub=1.5)
boxplot(dataUpTo40$TotalSales,data=dataUpTo40, main="Sales bis zu 40 (ohne 0)", 
        xlab=paste(nrow(dataUpTo40),"Datens?tze"), ylab="TotalSales pro Urprojekt",
        cex.lab=1.5, cex.axis=1.5, cex.main=1.5, cex.sub=1.5)
par(mfrow=c(1,1))

summary(data_original$TotalSales)
summary(data$TotalSales)
summary(dataUpTo100$TotalSales)
summary(dataUpTo40$TotalSales)



# Good und Bad untersuchen
badplayer <- subset(dataVerlagsfrei1, SalesQuantil == "bad")
goodplayer <- subset(dataVerlagsfrei, SalesQuantil == "good")

# Wordcloud von good und bad nebeneinander (Wordcloud mit max 75 W?rter)
par(mfrow=c(1,2))
v_bad <- sort(colSums(badplayer[,17:length(badplayer)]), decreasing=TRUE)
d_bad <- data.frame(word=names(v_bad), freq=v_bad)
wordcloud(d_bad$word, d_bad$freq, max.words=20, random.color=TRUE, colors=rainbow(7))

v_good <- sort(colSums(goodplayer[,17:length(badplayer)]), decreasing=TRUE)
d_good <- data.frame(word=names(v_good), freq=v_good)
wordcloud(d_good$word, d_good$freq, max.words=20, random.color=TRUE, colors=rainbow(7))
par(mfrow=c(1,1))

# Plot der User mit meisten kummulierten Verk?ufe
#install.packages("reshape")
library(reshape)
#install.packages("reshape2")
library(reshape2)

transform <- melt(dataVerlagsfrei1[,c(4,13)], id.vars="PublisherID", measure.vars = "TotalSales")
transform2 <- dcast(transform, PublisherID ~ variable, sum)
transform3 <- tail(transform2[order(transform2[,2]),],10)
transform3$PublisherID <- as.character(transform3$PublisherID)
qplot(transform3$PublisherID, transform3$TotalSales, geom="bar", stat="identity", ylab="Sales", xlab="PublisherID") + coord_flip()

QuotePlot <- melt(dataVerlagsfrei1[,c(13,16)], id.vars="PublisherID", measure.vars = "Quote")
QuotePlot2 <- dcast(QuotePlot, PublisherID ~ variable, mean)
#QuotePlot3 <- tail(QuotePlot2[order(QuotePlot2[,2]),],10)
QuotePlot2$PublisherID <- as.character(QuotePlot2$PublisherID)
QuotePlot3 <- QuotePlot2[which(QuotePlot2$PublisherID %in% transform3$PublisherID),]
qplot(QuotePlot3$PublisherID, QuotePlot3$Quote, geom="bar", stat="identity", ylab="Quote", xlab="PublisherID") + coord_flip()

# Nur Keywordsdataframe f?r PCA und NA Werte f?r 6 Keywords welche durch Verlagsuser entfernen rausgeflogen sind entfernen
colnames(data)[1:19]
mydata <- dataVerlagsfrei1[,19:length(dataVerlagsfrei1)]
variables.to.remove = c("harenberg",
                        "heye",
                        "kitsurfen",
                        "kvh",
                        "sportkalender",
                        "weingarten")
dataVerlagsfrei1 <- Remove.Redundant.Variables(raw.dataset = dataVerlagsfrei1,variables.to.remove = variables.to.remove)
rm(variables.to.remove)

if(FALSE){
  (Start <- Sys.time())
  # Estimate 1,02471 hours for 4283 Keywords
  my.prc <- prcomp(mydata, center=T, scale=T)
  (End <- Sys.time())
  (Duration <- End - Start)
  
  var.my.prc <- my.prc$sdev^2
  head(var.my.prc, 10)
  kaisergrenze <- length(subset(var.my.prc, var.my.prc > 1))
  
  screeplot(my.prc, type="line", main="Scree Plot", npc=min(40, kaisergrenze))
  
  #class(my.prc)
  #ls(my.prc)
  #summary(my.prc)
}

if(FALSE){
  #install.packages("psych")
  library(psych)
  
  (Start <- Sys.time())
  # Estimate 3,5 min for 345 Keywords
  fit1 <- principal(mydata, nfactors=20, rotate="varimax")
  (End <- Sys.time())
  (Duration <- End - Start)
  rm(Start, End, Duration)
  dataVerlagsfrei1PCA <- data.frame(dataVerlagsfrei1[,1:19],fit1$scores)
}

library(plyr)
# Keywords for Principal Components 1-20 in Liste
load = fit1$loadings
PCA_Keywords <- list()
i <- 1
for(i in 1:20){
  sorted.loadings = load[order(load[,i],decreasing = TRUE),i]
  PCA_Keywords[i] <- ldply(subset(sorted.loadings, sorted.loadings > 0.3))
}
rm(i, load, sorted.loadings)

PCA_Keywords
# W?rter Gruppen aus PCA_Keywords Sinn geben
colnames(dataVerlagsfrei1PCA)[20] <- paste("ErholungLife")
colnames(dataVerlagsfrei1PCA)[21] <- paste("Stadttourismus")
colnames(dataVerlagsfrei1PCA)[22] <- paste("Nordseek?ste")
colnames(dataVerlagsfrei1PCA)[23] <- paste("Pflanzen")
colnames(dataVerlagsfrei1PCA)[24] <- paste("Meer")
colnames(dataVerlagsfrei1PCA)[25] <- paste("City")
colnames(dataVerlagsfrei1PCA)[26] <- paste("EnglischNature")
colnames(dataVerlagsfrei1PCA)[27] <- paste("Jahrezeit")
colnames(dataVerlagsfrei1PCA)[28] <- paste("Afrika")
colnames(dataVerlagsfrei1PCA)[29] <- paste("Essen")
colnames(dataVerlagsfrei1PCA)[30] <- paste("USA")
colnames(dataVerlagsfrei1PCA)[31] <- paste("abstrakt")
colnames(dataVerlagsfrei1PCA)[32] <- paste("Deutschland")
colnames(dataVerlagsfrei1PCA)[33] <- paste("Berge")
colnames(dataVerlagsfrei1PCA)[34] <- paste("Tiere")
colnames(dataVerlagsfrei1PCA)[35] <- paste("AsienReligion")
colnames(dataVerlagsfrei1PCA)[36] <- paste("Farben")
colnames(dataVerlagsfrei1PCA)[37] <- paste("Erotik")
colnames(dataVerlagsfrei1PCA)[38] <- paste("Zitate")
colnames(dataVerlagsfrei1PCA)[39] <- paste("Schottland")


if(FALSE){
  # Sonstiges ############################################################################################
  # Clean up ##################################################
  head(mydata)
  dim(mydata)
  install.packages("caret")
  library(caret)
  nzv <- nearZeroVar(mydata, saveMetrics = TRUE)
  print(paste('Range:' ,range(nzv$percentUnique)))
  head(nzv)
  dim(nzv[nzv$percentUnique > 0.011,])
  # ENDE - Clean up ##################################################
  
  # plot correlation ##################################################
  install.packages("gclus")
  library(gclus)
  my.abs = abs(cor(mydata[, 1:7]))
  my.colors = dmat.color(my.abs)
  my.ordered = order.single(cor(mydata[, 1:7]))
  cpairs(mydata[, 1:7],my.ordered,panel.colors=my.colors,gap=0.5)
  # ENDE - plot correlation ##################################################
  
  # plot single Principial Components ##################################################
  install.packages("Hmisc")
  library(Hmisc)
  
  # PC1
  load = fit1$loadings
  sorted.loadings = load[order(load[,20]),20]
  Main="Loading Plot for PC14"
  xlabs="Variable Loadings"
  dotplot(sorted.loadings, main=Main, xlab=xlabs, cex=1.5,col="red")
  
  # PC2
  load = fit1$loadings
  sorted.loadings = load[order(load[,2]),2]
  Main="Loading Plot for PC2"
  xlabs="Variable Loadings"
  dotplot(sorted.loadings, main=Main, xlab=xlabs, cex=1.5,col="red")
  
  biplot(my.prc,cex=c(1,0.7))
  # ENDE - plot single Principial Components ##################################################
  # ENDE - Sonstiges ############################################################################################
}



# Combine names for regression
#x <- (colnames(mynewdata)[17:length(mynewdata)])
#y <- "TotalSales" 
#mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
#summary(lm(mymodel, data=mynewdata))



# Regression ##############################################################################################

# Reg1
lmfit <- lm(mynewdata$TotalSales ~ mynewdata$UsedKeywordsTotal)
summary(lmfit)

mynewdata$PublisherID <- as.character(mynewdata$PublisherID)
data$PublisherID <- as.character(data$PublisherID)

# Reg2
x <- (colnames(mynewdata)[c(6,12:length(mynewdata))])
y <- "TotalSales" 
mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
lmfit_PC1to20 <- lm(mymodel, data=mynewdata)
summary(lmfit_PC1to20)
coef_PC1to20 <- summary(lmfit_PC1to20)$coefficients
subset(coef_PC1to20,coef_PC1to20[,4]<0.01)

# Reg3!!!!!!!!!!!
x <- (colnames(dataVerlagsfrei2)[c(7,10,15,19:length(dataVerlagsfrei2))])
y <- "av.Sales" 
mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
lmfit_allkeywords <- lm(mymodel, data=dataVerlagsfrei2)
summary(lmfit_allkeywords)
coef_allkeywords <- summary(lmfit_allkeywords)$coefficients
subset(coef_allkeywords,coef_allkeywords[,4]<0.01)

# stepwise regression -> exclude unimportant parameters
if(false){
  (Start <- Sys.time())
  # Estimate x min for 345 Keywords
  step <- step(lmfit_allkeywords, direction="both")
  (End <- Sys.time())
  (Duration <- End - Start)
  rm(Start, End, Duration)
}

# Prediction und Vergleich von Sch?tzungen
prediction <- predict(lmfit,data)
View(prediction)
View(cbind(lmfit$residuals, lmfit$fitted.values, data$TotalSales))

# Test for specific keyword "downhill"
downhilldata <- subset(dataVerlagsfrei1, downhill>0)
downhilldata.good <- subset(downhilldata, SalesQuantil=="good")
downhilldata.bad <- subset(downhilldata, SalesQuantil=="bad")
summary(downhilldata.good)
summary(downhilldata$TotalSales)
View(downhilldata)


# Decision Tree ##############################################################################################
#install.packages("rpart")
library(rpart)
#http://www.statmethods.net/advstats/cart.html
#http://www.r-bloggers.com/a-brief-tour-of-the-trees-and-forests/

data_good_bad <-  subset(dataVerlagsfrei1, SalesQuantil!="medium")
data_good_bad$PublisherID <- as.factor(data_good_bad$PublisherID)

dataVerlagsfrei2$PublisherID <- as.factor(dataVerlagsfrei2$PublisherID)
dataVerlagsfrei2$Kategorie <- as.factor(dataVerlagsfrei2$Kategorie)
dataVerlagsfrei2$Veroeffentlichungsmonat <- as.factor(dataVerlagsfrei2$Veroeffentlichungsmonat)
dataVerlagsfrei2$Veroeffentlichungsquartal <- as.factor(dataVerlagsfrei2$Veroeffentlichungsquartal)
names(dataVerlagsfrei2)[8]<-paste("Quarter")
names(dataVerlagsfrei2)[9]<-paste("Month")

for(i in 1:length(data_good_bad$Titel))
{
  if (data_good_bad$Quote[i] <= 0.5)
  {
    data_good_bad$Quote[i] <- 3
  }
  else if(data_good_bad$Quote[i] >= 1.5)
  {
    data_good_bad$Quote[i] <- 1
  }
  else(data_good_bad$Quote[i] <- 2)
}


for(i in 1:length(dataVerlagsfrei2$Titel))
{
  if (dataVerlagsfrei2$Quote[i] <= 0.5)
  {
    dataVerlagsfrei2$Quote[i] <- 3
  }
  else if(dataVerlagsfrei2$Quote[i] >= 1.5)
  {
    dataVerlagsfrei2$Quote[i] <- 1
  }
  else(dataVerlagsfrei2$Quote[i] <- 2)
}
dataVerlagsfrei2$Quote <- as.factor(dataVerlagsfrei2$Quote)


for(i in 1:length(mynewdata$Titel))
{
  if (mynewdata$Quote[i] <= 0.5)
  {
    mynewdata$Quote[i] <- 3
  }
  else if(mynewdata$Quote[i] >= 1.5)
  {
    mynewdata$Quote[i] <- 1
  }
  else(mynewdata$Quote[i] <- 2)
}

mynewdata_good_bad <-  subset(mynewdata, SalesQuantil!="medium")

# Tree 1 & 2
x <- (colnames(data)[c(16,17:length(data))])
y <- "SalesQuantil" 
mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
fit1 <- rpart(mymodel, method="class", data = data_good_bad, control=list(minsplit=1000, cp=0.0035))
?rpart
#fit2 <- rpart(mymodel, method="class", data = data)

# Tree 3
x <- (colnames(dataVerlagsfrei2)[c(7,10,15,19:length(dataVerlagsfrei2))])
y <- "av.Sales" 
mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
fit3 <- rpart(mymodel, method="anova", control=list(minsplit=2, cp=0.001),data = dataVerlagsfrei2)

# Tree 4
x <- (colnames(mynewdata)[17:length(mynewdata)])
y <- "TotalSales" 
mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
fit4 <- rpart(mymodel, method="anova", control=list(minsplit=2, cp=0.001), data = mynewdata)

# Tree 5
x <- (colnames(mynewdata)[17:length(mynewdata)])
y <- "SalesQuantil" 
mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
fit5 <- rpart(mymodel, method="class", control=list(minsplit=5), data = mynewdata_good_bad)
?rpart
# Plot last progressed tree
#http://stackoverflow.com/questions/2597310/how-do-i-interpret-rpart-splits-on-factor-variables-when-building-classification/3840211#3840211
plot(fit3, uniform=TRUE, main="Classification Tree for Sales")
text(fit3, use.n=TRUE, all=TRUE, cex=.6, pretty=1)
#post(fit, file = "c:/tree.ps", title = "Classification Tree for Sales")
install.packages("maptree")
library(maptree)
draw.tree(fit3)

# Prediction und Vergleich von Sch?tzungen
prediction <- predict(fit3,dataVerlagsfrei2)
View(prediction)
View(cbind(fit3$residuals, fit3$fitted.values, dataVerlagsfrei2$av.Sales))


# Another Tree Type
install.packages("partykit")
library("partykit")
?ctree
fit <- ctree(TotalSales ~ PC1 + PC2 + PC3 + PC4 + 
               PC5 + PC6 + PC7 + PC8 + PC9 + PC10 + 
               PC11 + PC12 + PC13 + PC14 + PC15 + 
               PC16 + PC17 + PC18 + PC19 + PC20, data = mynewdata)


# Random Forest with PCA Components and max Sale "Garfield" raus
subset(mynewdata,mynewdata$TotalSales == max(mynewdata$TotalSales))
rfTree <- subset(mynewdata, !is.na(TotalSales))
rfTree <- subset(mynewdata, mynewdata$TotalSales != max(mynewdata$TotalSales))
x <- (colnames(rfTree)[17:length(rfTree)])
y <- "TotalSales" 
mymodel <- as.formula(paste(y, paste(x, collapse="+"), sep="~"))
rfTreePlot <- ctree(mymodel, data = rfTree, 
               controls = ctree_control(maxsurrogate = 3, mincriterion = 0.9999))
plot(rfTreePlot)  #default plot, some crowding with N hidden on leafs


# Something else
print(fit)
plot(fit)

printcp(fit) # display the results 
plotcp(fit) # visualize cross-validation results 
summary(fit) # detailed summary of splits


#################Prediction

new <- dataVerlagsfrei1[1,]
filename <- paste("Data to predict", format(Sys.time(), "%Y-%m-%d_%H-%M"), ".csv", sep = "")
setwd("Z:/Projekte Analytics/Calvendo/Rohdaten/CSV-Files Output")
write.csv(new, file = filename)
setwd("Z:/Projekte Analytics/Calvendo/Rohdaten")
